#!/usr/bin/env python

import sys
import numpy as np
import random 

# Magic random seeds: https://keras.io/getting-started/faq/#how-can-i-obtain-reproducible-results-using-keras-during-development
seed = random.randrange(10000)
print('Random seed: {}'.format(seed))
np.random.seed(seed)
random.seed(seed)
import tensorflow as tf
tf.set_random_seed(seed)

import gym
import copy, argparse
from collections import deque
from timeit import default_timer as timer

import keras
from keras.layers import Dense, Activation, Input, Lambda
from keras.layers.merge import Add
from keras.models import Model, Sequential, load_model
from keras.optimizers import Adam
from keras.callbacks import LearningRateScheduler
from keras.callbacks import TensorBoard
from keras import backend as K
import tensorflow as tf
import pdb

run_id = int(np.random.random()*1000)
print('----\nrun_id: {}\nThis is used in saved model names to make sure we don\'t overwrite other ones\n----'.format(run_id))

class QNetwork():
    def __init__(self, env, model_type, activation, lr, lr_schedule='constant', tensorboard=False): 
        """
        Initialize Q-network and training variables

        Args: 
            env (openai gym env)
            model_type (str): Q-network model type. Possible values: 
                'linear', 'linear_w_replay', 'dqn', 'dueling_dqn'
            activation (str): type of activation to use in model
            tensorboard (bool): to log metrics for tensorboard or not
        """

        self.state_size = env.observation_space.shape[0]
        self.action_size = env.action_space.n # note that this assumes discrete action space

        self.model = self._build_model(model_type, activation)
        self.optimizer = Adam(lr=lr) #, decay=0.00003) # TODO re-evaluate decay rate
        self.model.compile(optimizer=self.optimizer, loss='mse')

        self.callbacks = []

        if tensorboard:
            # `tensorboard --logdir=logs/` in same directory, `127.0.0.1:6006` in browser
            # TODO figure out how to log average reward
            logger = TensorBoard(log_dir="./logs")
            self.callbacks.append(logger)

    def _build_model(self, model_type, activation):
        if model_type == 'linear':
            model = Sequential([
                Dense(self.action_size, input_shape=(self.state_size,), activation='linear', name="q_values"),
            ])
        elif model_type == 'dqn_1hidden':
            n_hidden = 30
            model = Sequential([
                Dense(n_hidden, input_shape=(self.state_size,), activation=activation, name="hidden"), 
                Dense(self.action_size, activation='linear', name="q_values"),
            ])
        elif model_type == 'dqn_2hidden':
            n_hidden = 30
            model = Sequential([
                Dense(n_hidden, input_shape=(self.state_size,), activation=activation, name="hidden1"), 
                Dense(n_hidden, activation=activation, name="hidden2"), 
                Dense(self.action_size, activation='linear', name="q_values"),
            ]) 
        elif model_type == 'dqn_3hidden':
            n_hidden = 128
            model = Sequential([
                Dense(n_hidden, input_shape=(self.state_size,), activation=activation, name="hidden1"), 
                Dense(n_hidden, activation=activation, name="hidden2"), 
                Dense(n_hidden, activation=activation, name="hidden3"), 
                Dense(self.action_size, activation='linear', name="q_values"),
            ]) 
        elif model_type == 'dueling_dqn':
            n_hidden_common = 30

            inputs = Input(shape=(self.state_size,), name="input")
            net = Dense(n_hidden_common, activation=activation, name="fc1")(inputs)

            value = Dense(1, activation='linear', name="value")(net)

            advantage = Dense(self.action_size, activation='linear', name="advt1")(net)
            advantage = Lambda(lambda adv: adv - K.mean(adv), name="advt2")(advantage) # why does this subtraction work? 

            Qvalues = Add(name="q_values")([value, advantage])

            model = Model(inputs=inputs, outputs=Qvalues)
        elif model_type == 'dueling_dqn_3hidden':
            n_hidden_common = 128

            inputs = Input(shape=(self.state_size,), name="input")

            net = Dense(n_hidden_common, input_shape=(self.state_size,), activation=activation, name="hidden1")(inputs)
            net = Dense(n_hidden_common, activation=activation, name="hidden2")(net)
            net = Dense(n_hidden_common, activation=activation, name="hidden3")(net)

            value = Dense(1, activation='linear', name="value")(net)

            advantage = Dense(self.action_size, activation='linear', name="advt1")(net)
            advantage = Lambda(lambda adv: adv - K.mean(adv), name="advt2")(advantage) # why does this subtraction work? 
            
            Qvalues = Add(name="q_values")([value, advantage])

            model = Model(inputs=inputs, outputs=Qvalues)
        else:
            raise ValueError('model_type \'{}\' not valid'.format(model_type))

        return model

    def predict(self, state):
        """
        Run Q-network forward given a state to get state-action values. 
        Note that the reshapes here are because keras model expects (1, n), 
        while in the rest of the code it's easier to work with (n, ) arrays

        Args:
            state (np.array)

        Returns:
            Q_sa (np.array) : state-action values
        """
        state = state.reshape((-1, state.size))
        Q_sa = self.model.predict(state)
        return Q_sa.reshape((Q_sa.size,)) 

    def fit(self, state, target):
        """
        Run one epoch of stochastic gradient descent on Q-network parameters
        
        Args:
            state (np.array)
            target (np.array): desired state-action (Q) values 

        Returns:
            loss (double)
        """
        if state.size == self.state_size:
            state = state.reshape((-1, state.size))
            target = target.reshape((-1, target.size))
        history = self.model.fit(state, target, epochs=1, verbose=False, shuffle=False, callbacks=self.callbacks)

        return history.history['loss'][0]

    def save_model(self, name):
        """
        Helper function to save your model / weights.

        Args:
            name (str): file name without extension
        """
        print('Saving model to ' + name + '.h5')
        self.model.save(name + '.h5')

    def load_model(self, model_file):
        """
        Helper function to load an existing model.

        Args:
            model_file (str): file path to h5 file
        """
        print('Loading model from ', model_file)
        self.model = load_model(model_file)

    def get_activations(self, layer_name, X_input):
        """
        Get activations of hidden layers in network for a given input 

        From: https://github.com/keras-team/keras/issues/41#issuecomment-219262860

        I used this to double-check dueling network implementation: 
            advt1 = self.Q.get_activations("advt1", state)
            value = self.Q.get_activations("value", state)
            q_values = self.Q.get_activations("q_values", state)
            print('network output q_values: ', q_values)
            print('manually calculated q_values: ', (advt1 - np.mean(advt1)) + value )

        Args:
            layer_name (str): layer name as defined in _build_model
            X_input (np.array): input to pass to network. usually one vector

        Returns:
            activation (np.array): output of layer for input X_input
        """
        X_input = np.reshape(X_input, (-1, self.state_size))
        names = [layer.name for layer in self.model.layers]
        if layer_name not in names:
            raise ValueError('Desired layer not in model. Available layers: ', names)
        get_activations = K.function([self.model.layers[0].input, K.learning_phase()], 
                                     [self.model.layers[names.index(layer_name)].output])
        activations = get_activations([X_input,0])
        return activations

class Replay_Memory():

    def __init__(self, burn_in_memory, memory_size=50000):
        """
        Stores past transitions of agent. 
        Burn in episodes define the number of episodes that are written into the memory from the 
        randomly initialized agent.

        Args:
            burn_in_memory : list of transitions tuples (state, action, next_state, reward, terminal)
            memory_size (int): max size of memory buffer, after which old elements are replaced
        """

        self.memory = deque(maxlen=memory_size)
        self.memory.extend(burn_in_memory)

        self.good_memory = deque(maxlen=2000)

    def sample_batch(self, batch_size):
        """
        Returns a batch of randomly sampled transitions from memory. 
        Note that this assumes the memory has at least batch_size transitions recorded (i.e. burn_in_size > batch_size)

        Args:
            batch_size (int)

        Returns:
            minibatch: list of transition tuples
        """
        # Trick to get agent to train itself on successful episodes, if it has had one
        if len(self.good_memory) >= batch_size:
            if np.random.rand() > 0.5: 
                # print('Sampling transitions from successful runs')
                return random.sample(self.good_memory, batch_size)
            
        return random.sample(self.memory, batch_size)

    def append(self, transition):
        """
        Appends transition to the memory, overwriting oldest transition if memory is full  

        Args:
            transition: tuple (state, action, next_state, reward, terminal)
        """
        self.memory.append(transition)

    def remember_success(self, past_k_transitions):
        """
        Trick to get agent to train itself on successful episodes, if it has encountered one so far
        """
        self.good_memory.extend( list(self.memory)[-int(past_k_transitions):] )
        print('Remembered success; self.good_memory is now of length {}'.format(len(self.good_memory)))


class DQN_Agent():

    def __init__(self, environment_name, model_type, lr, render, activation, gradient_clip, replay_freq, epsilon_start, epsilon_end, model_path=None):
        """
        Create an instance of DQN agent, including Q-network and replay memory. 
        
        Args:
            environment_name (str): openai gym environment name 
            model_type (str): model structure of Q-network
        """

        self.env = gym.make(environment_name)
        self.action_size = self.env.action_space.n
        self.state_size = self.env.observation_space.shape[0] # TODO change this for image inputs
        self.render = render

        self.model_type = model_type
        self.lr_schedule = 'constant' # TODO use this 
        self.replay_freq = replay_freq
        self.batch_size = 32
        self.save_checkpoints = False

        self.lr = lr
        self.epsilon_start = epsilon_start
        self.epsilon_end = epsilon_end
        self.clip_gradients = gradient_clip

        if environment_name == "MountainCar-v0":
            self.ou_state = 1   

        print('-'*50, '\nMaking agent with parameters:')
        print('env: {}\nmodel: {}\nlr: {}\nepsilons: [{}, {}]\ngradient_clip: {}\n\nlr schedule: {}\nactivation: {}\nreplay_freq: {}\nbatch_size: {}\nRandom seed: {}'.format(
               environment_name, model_type, self.lr, self.epsilon_start, self.epsilon_end, self.clip_gradients, self.lr_schedule, activation, self.replay_freq, self.batch_size, seed))
        print('-'*50)
        # Seed is set at top of this file, but doesn't really work right now

        self.LOG_EVERY_N_EPISODES = 10
        self.TEST_EVERY_N_EPISODES = 50

        self._set_env_parameters(environment_name)

        self.Q = QNetwork(self.env, model_type, activation, self.lr)
        if model_path is not None:
            print('loading model before burn-in')
            self.Q.load_model(model_path)

        if self.replay_freq != 'none': 
            if environment_name == 'CartPole-v0':
                burn_in_size = 32
                memory_size = 2000
            else: 
                burn_in_size = 10000
                memory_size = 50000
            burn_in_buffer = self._burn_in_memory(burn_in_size)
            self.replay_memory = Replay_Memory(burn_in_buffer, memory_size)

    def _set_env_parameters(self, environment_name):
        """
        Set environment-specific training parameters.

        Args:
            environment_name (str)
        """
        if environment_name == 'MountainCar-v0':
            self.gamma = 1
            self.max_episodes = 3000
            self.max_iterations = float("inf")
        elif environment_name == 'CartPole-v0':
            self.gamma = 0.99
            self.max_episodes = float("inf")
            self.max_iterations = 300000 # 1000000
        else:
            ValueError('environment_name \'{}\' not valid'.format(environment_name))

    def _burn_in_memory(self, burn_in):
        """ 
        Get list of transitions from random policy to initialize replay memory 
        with a burn_in number of episodes / transitions. 

        Args:
            burn_in (int): number of transitions to record to burn_in_memory

        Returns:
            burn_in_memory (np.array): list of transition tuples of length burn_in
        """

        transition_count = 0
        burn_in_memory = []
        while transition_count < burn_in:
            done = False
            obs = self.env.reset()

            while not done:
                last_state = np.copy(obs) # does this need to be deep copy? 

                Qsa = self.Q.predict(last_state)
                action = self.epsilon_greedy_policy(Qsa, 0.05)
                obs, reward, done, info = self.env.step(action)
                burn_in_memory.append((last_state, action, reward, obs, done))
                
                transition_count += 1
                if(transition_count >= burn_in): break

        return burn_in_memory

    def epsilon_greedy_policy(self, q_values, epsilon):
        if np.random.rand() > epsilon:
            return np.argmax(q_values)
        else:
            return self.env.action_space.sample()

    def greedy_policy(self, q_values):
        return np.argmax(q_values)

    def ou_policy(self, q_values, epsilon):
        """
        Epsilon-greedy policy with random uniform sample of action space replaced by
        Ornstein-Uhlenbeck process

        Encourages continuous exploration (instead of jittering back-and-forth, which goes nowhere in the mean)
        """
        theta = 0.1 # How quickly the value regresses to mean
        sigma = 0.2 # Weight of the noise
        mu = 1 # MountainCar has 0, 1, 2 actions. so mean is 1 (don't do anything)
        min_action, max_action = 0, 2
        
        # Increment noise state
        dx = theta * (mu - self.ou_state) + sigma * np.random.randn(1)[0]
        self.ou_state += dx

        if np.random.rand() > epsilon:
            return np.argmax(q_values)
        else:
            # Round and clip to a valid action
            return int(round( min(max(self.ou_state, min_action), max_action) ))

    def train(self):
        """ 
        Train Q-network. Interact with environment and save transition to replay memory (if using)
        """

        print('Training...')
        n_episodes = 0 
        n_iterations = 0
        performance_log = []
        last_k_scores = deque(maxlen=self.LOG_EVERY_N_EPISODES)

        eps_start, eps_end = self.epsilon_start, self.epsilon_end
        epsilon = eps_start

        while (n_episodes < self.max_episodes) and (n_iterations < self.max_iterations):
            # Start new episode
            done = False
            obs = self.env.reset()
            total_reward = 0

            while not done:
                if self.render: self.env.render()

                # Choose an action, with level of exploration determined by epsilon
                state = obs
                Qsa = self.Q.predict(state)
                if self.env.spec.id == 'MountainCar-v0':
                    action = self.ou_policy(Qsa, epsilon)
                else: 
                    action = self.epsilon_greedy_policy(Qsa, epsilon)

                # Take action and record results
                obs, reward, done, info = self.env.step(action)
                next_state = obs
                total_reward += reward

                if self.replay_freq == 'every_iter': 
                    self.replay_memory.append((state, action, reward, next_state, done))
                    loss = self.experience_replay()
                elif self.replay_freq == 'every_episode': 
                    self.replay_memory.append((state, action, reward, next_state, done))
                elif self.replay_freq == 'none':    
                    target = reward if done else reward + self.gamma * np.max(self.Q.predict(next_state))
                    Qsa = self.Q.predict(state)
                    if self.clip_gradients:
                        td_error = target - Qsa[action]
                        target = Qsa[action] + np.clip(td_error, -1, +1)                        
                    Qsa[action] = target
                    loss = self.Q.fit(state, Qsa)
                else: # (Kazu) not ideal but putting this here for now
                    ValueError('replay_freq \'{}\' invalid'.format(self.replay_freq)) 

                n_iterations += 1
                if n_iterations > self.max_iterations: break

            last_k_scores.append(total_reward)

            # Trick to get mountaincar to maximize learning from successful episodes
            if self.env.spec.id == "MountainCar-v0":
                solved = (total_reward<0 and total_reward > -199)
                if solved:
                    # Use the fact that the reward is -n_iter for mountaincar
                    self.replay_memory.remember_success(past_k_transitions=abs(total_reward))

            if self.replay_freq == 'every_episode': 
                loss = self.experience_replay()   

            # Log policy performance     
            # Note that this test uses a more greedy policy than the policy we're using to explore/learn
            if(n_episodes%self.TEST_EVERY_N_EPISODES == 0):
                avg_reward, std_reward = self.test()
                performance_log.append(avg_reward)
                print('Average, std test reward: {}, {:.2f}'.format(avg_reward, std_reward))
                solved = (avg_reward>195) or (avg_reward<0 and avg_reward > -110) # first is for cartpole, second is for mountain car
                if solved:
                    print('Environment solved!')
                    break
                save_name = 'models/{}-{}-{}-episode-{}'.format(self.model_type, self.env.spec.id, run_id, n_episodes)
                print('Saving to: ', save_name)
                self.Q.save_model(save_name)

            # Print progress
            if(n_episodes%self.LOG_EVERY_N_EPISODES == 0):
                print('episodes: {}/{}, iters: {}/{}, R: {:.1f}, loss: {:.2f}, eps: {:.2f}'.format(
                        n_episodes, self.max_episodes, n_iterations, self.max_iterations,
                        sum(last_k_scores)/len(last_k_scores), loss, epsilon))

            # Decay epsilon
            # We occasionally used exponential decay for epsilon on cartpole
            # if self.env.spec.id == "CartPole-v0":
                # epsilon = max(epsilon * 0.995, eps_end)
            # else:
            eps_phase = min(n_iterations/100000, 1)
            epsilon = eps_start + (eps_end-eps_start) * eps_phase

            n_episodes += 1

        return performance_log

    def experience_replay(self):
        """ 
        Run batch of Q-learning steps from transitions in replay memory.

        Returns:
            average loss of Q-network

        See git history for original incremental version (pass each episode to model.fit individually)
        """
        minibatch = self.replay_memory.sample_batch(self.batch_size)

        ### Passing minibatch one at a time
        # for state, action, reward, next_state, done in minibatch:
        #     target = reward
        #     if not done:
        #         target = (reward + self.gamma * np.amax(self.Q.predict(next_state)))
        #     target_q = self.Q.predict(state)
        #     target_q[action] = target
        #     loss = self.Q.fit(state, target_q)

        # ### Passing entire minibatch to train model at once
        minibatch = self.replay_memory.sample_batch(self.batch_size)
        minibatch_x = np.zeros((len(minibatch), self.state_size))
        minibatch_y = np.zeros((len(minibatch), self.action_size))

        for i in range(len(minibatch)): 
           state, action, reward, next_state, terminal = minibatch[i]
           target = reward if terminal else reward + self.gamma * np.max(self.Q.predict(next_state))
           Qsa = self.Q.predict(state)
           Qsa[action] = target
           minibatch_x[i, :] = state
           minibatch_y[i, :] = Qsa

        loss = self.Q.fit(minibatch_x, minibatch_y)

        return loss

    def test(self, model_file=None, record=False):
        """ 
        Evaluate the average performance of agent over 20 episodes.
        Use an epsilon-greedy policy with eps=0.05    

        Args:
            model_file (str): [Optional] filepath for pre-trained model h5 file

        Returns:
            Mean and std of rewards over N episodes
        """
        if model_file is not None:
            self.Q.load_model(model_file)

        if record:
            self.env = gym.wrappers.Monitor(self.env, '/tmp/{}-{}'.format(self.model_type, self.env.spec.id), force=True)

        n_test_episodes = 20
        episode_rewards = []
        for episode in range(n_test_episodes):
            total_reward = 0
            done = False
            obs = self.env.reset()

            while not done:
                if self.render: self.env.render()

                action = self.epsilon_greedy_policy(self.Q.predict(obs), 0.05)
                obs, reward, done, info = self.env.step(action)

                min_x, max_x = min(min_x, obs[0]), max(max_x, obs[0])
                total_reward += reward
            episode_rewards.append(total_reward)

        episode_rewards = np.array(episode_rewards)

        return np.mean(episode_rewards), np.std(episode_rewards)


def parse_arguments():
    parser = argparse.ArgumentParser(description='Deep Q Network Argument Parser')
    parser.add_argument('--train', dest='train', type=int, default=1)
    parser.add_argument('--render', dest='render', type=int, default=0)
    parser.add_argument('--model_path', dest='model_path', type=str, default='none') 
    
    parser.add_argument('--env', dest='env', type=str) # 'CartPole-v0', 'MountainCar-v0'
    parser.add_argument('--model_type', dest='model_type', type=str) # 'linear', 'dqn_1hidden', 'dqn_2hidden', 'dueling_dqn'

    # Parameters to do grid search over
    parser.add_argument('--learning_rate', dest='learning_rate', type=float) # 0.0001, 0.00025, 0.001
    parser.add_argument('--epsilon_start', dest='epsilon_start', type=float) # 1.0, 0.5
    parser.add_argument('--epsilon_end', dest='epsilon_end', type=float) # 0.25, 0.05
    parser.add_argument('--gradient_clip', dest='gradient_clip', type=int) # 0, 1

    # Parameters we're keeping constant for now
    parser.add_argument('--activations', dest='activations', type=str, default='relu') # 'relu', 'tanh'
    parser.add_argument('--replay_freq', dest='replay_freq', type=str, default='every_iter') # 'none', 'every_iter', 'every_episode'

    return parser.parse_args()

def main(args):
    args = parse_arguments()

    # Setting the session to allow growth, so it doesn't allocate all GPU memory. 
    gpu_ops = tf.GPUOptions(allow_growth=True)
    config = tf.ConfigProto(gpu_options=gpu_ops)
    sess = tf.Session(config=config)

    # Setting this as the default tensorflow session. 
    keras.backend.tensorflow_backend.set_session(sess)

    ### Choose model and environment type
    if args.train:
        if args.model_path == 'none':
            agent = DQN_Agent(environment_name=args.env, model_type=args.model_type, lr=args.learning_rate, render=args.render, 
                          activation=args.activations, gradient_clip=args.gradient_clip, replay_freq=args.replay_freq, 
                          epsilon_start=args.epsilon_start, epsilon_end=args.epsilon_end) 
        else:
            agent = DQN_Agent(environment_name=args.env, model_type=args.model_type, lr=args.learning_rate, render=args.render, 
                          activation=args.activations, gradient_clip=args.gradient_clip, replay_freq=args.replay_freq, 
                          epsilon_start=args.epsilon_start, epsilon_end=args.epsilon_end, model_path=args.model_path)             
        performance_log = agent.train()
        print('Performance log: ', performance_log)
        agent.Q.save_model('models/{}-{}-{}-{}-{}-{}-{}'.format(args.model_type, args.env, run_id, args.learning_rate, args.epsilon_start, args.epsilon_end, args.gradient_clip))
        print('Average reward: ', agent.test())
    else:
        agent = DQN_Agent(args.env_type, model_type=args.model_type, render=args.render) 
        print('Average reward: ', agent.test('models/{}-{}.h5'.format(args.model_type, args.env_type)))

if __name__ == '__main__':
    main(sys.argv)

