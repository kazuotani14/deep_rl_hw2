#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
from keras.models import Model, Sequential, load_model


# For cartpole, n_states = 4, n_actions = 2
# states: (x, dx, theta, dtheta), +-(2.4, inf, 41, inf)

# Query network at grid over (theta, dtheta) values
thetas = np.arange(-2.4, 2.4, 0.1)
dthetas = np.arange(-2, 2, 0.1)
states = [[0, 0, theta, dtheta] for theta in thetas for dtheta in dthetas]
states = np.array(states)

xs = np.arange(-1.2, 0.5, 0.1)
dxs = np.arange(-2, 2, 0.1)
states = [[x, dx] for x in xs for dx in dxs]
states = np.array(states)

def get_qvalues_from_model(X):
    # model_file = 'models/dqn-CartPole-v0.h5'
    # model_file = 'models/cartpole_dqn/dqn_1hidden-CartPole-v0-episode-650.h5'
    # model_file = 'models/dqn_2hidden-MountainCar-v0-398-episode-350.h5'
    # model_file = 'models/dqn_2hidden-MountainCar-v0-685-episode-1000.h5'
    # model_file = 'models/dqn_2hidden-MountainCar-v0-37-episode-500.h5'
    # model_file = 'models/dqn_2hidden-MountainCar-v0-687-episode-50.h5'
    model_file = 'models/mountaincar_dqn_remember/dqn_3hidden-MountainCar-v0-981-0.0001-1.0-0.05-0.h5'
    model = load_model(model_file)
    return model.predict(X)

def get_qvalues_from_linear(X):
    # Linear q-network 
    # W = np.array([[-0.60755907, -0.21477476, -0.41748399, -0.17601402], 
    #               [-0.60978078, -0.12562684,  0.06485164,  0.95163851]])
    W = np.array([[ 0.02681536, -0.54683283, -0.69713724, -0.16577171], 
                  [-0.30627268,  0.25155483,  0.605022,    0.78627031]])
    return np.matmul(X, W.T)

Qsa = get_qvalues_from_model(states)
# Qsa = get_qvalues_from_linear(states)

X, Y = states[:, 0], states[:, 1]
Q_a0 = Qsa[:, 0]
Q_a1 = Qsa[:, 1]

Zdiff = Q_a0-Q_a1

### 3D plot of individual action-value functions
# ax = plt.axes(projection='3d')
# ax.plot_trisurf(X, Y, Q_a0)
# ax.plot_trisurf(X, Y, Q_a1)
# plt.xlabel('x')
# plt.ylabel('dx')

### 2D plot 
colors = [] 
diff = np.absolute(Q_a0 - Q_a1)
max_diff = np.max(diff)
print('max, mean diff: {}, {}'.format(np.max(diff), np.mean(diff)))
for i in range(X.shape[0]):
    if Q_a0[i] >= Q_a1[i]:
        colors.append('r')
        # colors.append(( abs(Q_a0[i]-Q_a1[i])/max_diff, 0.0, 0.0)) 
    else:
        colors.append('b')
        # colors.append((0.0, 0.0, abs(Q_a0[i]-Q_a1[i])/max_diff ))

plt.scatter(X, Y, c=colors)
plt.xlabel('x')
plt.ylabel('dx')
plt.title('State value function for MountainCar, colored by dominant action')
# plt.savefig('mountaincar_value_func.png', bbox_inches='tight')

plt.show()

# plt.contourf(X, Y, Q_a0, 100) # TODO figure out meshgrid