import itertools
import pdb

def arg_permute(arg_list_dict):
    '''
    generate all possible permutations of a bash command taking selecting one parameter from each argument list
    '''
    permuted_arg_lists = list(itertools.product(*arg_list_dict.values()))
    args = arg_list_dict.keys()
    return permuted_arg_lists, args

def gen_bash_commands(arg_list_dict):
    '''
    Take in a list of all arguments and lists of their possible values.
    Generate bash commands to execute grid search over all possible combinations of arg values
    '''

    #generate all permutations of our arguments
    arg_lists, arg_names = arg_permute(arg_list_dict)

    # create bash commands
    bash_commands = []
    for arg_list in arg_lists:
        bash_command = "python3 DQN_Implementation.py "
        for i, arg_name in enumerate(arg_names):
            bash_command += "--{} {} ".format(arg_name, arg_list[i])
        bash_commands.append(bash_command)

    return bash_commands

if __name__ == "__main__":
    test_dict = {"learning_rate": [0.0001, 0.001, 0.00025, 0.00005],
                 "epsilon_start": [1.0, 0.5],
                 "epsilon_end": [0.2, 0.05],
                 "gradient_clip": [0,1],
                 "env": ["CartPole-v0"],
                 "model_type": ["dqn_2hidden"]}

    bash_commands = gen_bash_commands(test_dict)
    print(len(bash_commands))

    #write to file
    with open("DQN_grid_search.sh", "w") as f:
        for bash_command in bash_commands:
            f.write(bash_command+"\n")

