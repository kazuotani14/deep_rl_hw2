# Deep RL HW2 notes

### Current status

Only DQN for cartpole working?? Value function doesn't look right though

### To-do

Grid search over all possible parameters! 

* Put all parameters in arg_parser
* Write script to make all possible permutations, make job script, test without actually training
* Make sure we're log everything: print parameters in DqnAgent, **save checkpoints**, test performance, ???
* Triple-check implementations for Q-learning, replay memory, etc
* Find good set of parameters for DQN, then move on to others? Try for linear? 

Keep constant: 

* Model type
* Environment
* Random seed

Search parameters: 

* different epsilons for exploration. eps_start: [1.0, 0.5]. eps_end: [0.25, 0.05]
* gradient clipping: [with, without]
* learning rate: [0.0001, 0.00025, 0.001]

* learning rate schedule - [None, change over epochs, adjust when test performance applies]
* moving experience replay to once at end of episode: [each iteration, each episode]

* adding layers to DQN: [single hidden layer, two hidden layers]
* activation function of hidden layers: [relu, tanh]
* increasing batch size: [32, 128]
---

* Change replay memory to pre-allocate memory (like in http://srome.github.io/A-Tour-Of-Gotchas-When-Implementing-Deep-Q-Networks-With-Keras-And-OpenAi-Gym/)
    * might be necessary for Atari
* Check that dueling networks is working for multiple inputs (in experience replay), by printing out activations
* Try increasing batch size: "If batch size is too small, noisy will overpower signal."
* Logging
    * save checkpoints for 0/3, 1/3, 2/3, 3/3 training process - make sure these log files are overwritten, comment out until it's working
    * Log average reward to tensorboard using SummaryWriter 
        ```
        summary_writer = tf.summary.FileWriter(logdir=args.output)
        self.summary_writer.add_summary(tf.Summary(value=[ tf.Summary.Value(tag='name', simple_value=val)], global_step=iter)
        ```
    * Put per-iteration printouts on same line? 
* Figure out how to use Pittsburgh Supercomputer Center - currently getting stuck after tensorflow starts
* Try different epsilon-start, epsilon-end values
* Visualize value function (max of Q at each state) of solved cartpole dqn  - does it look linearly learnable? 
* When performance (average reward) plateaus, consider reducing epsilon (or tune decay parameter for Adam)
* Others' results: convergence for cartpole. dqn cartpole: 2000, ddqn: 1000 episodes

##### Second priority 

* Add bias to random search linear Q-network, for fair comparison
* Try numba to speed up - will it work with keras calls?  Where does the algorithm spend the most time? 
* Change dqn activations to sigmoid? does it matter?  Add more hidden layers/units?

### Notes

* **Both tasks are solvable with linear networks - see `random_search_solution.py`**
    * Is there any reason why our Q-learning algorithm wouldn't converge to one of these? 
    * Random search converges within 100 episodes! No wonder it's seeing as the obvious baseline for RL. Not sure if it will scale well with high dimensions

* Replay at end of episode? or iteration? paper looks like it does it at each iteration, some sample implementations look like at end of every episode
* We shouldn’t need to make a separate target network for simple environments. For atari, how to do it: https://github.com/keras-team/keras/issues/1765 

* It seems like the cartpole task would be easier for a Q-learning algorithm to solve, because it starts out roughly in the goal position and there’s more of a “gradient” in the rewards for the agent to learn from. i.e. If it takes a few actions that cause it to fail early, it will get less reward and adjust its Q-values to try the other action the next time it sees nearby states
    * The rewards for mountain-car task are very sparse - the whole process relies on the car getting to the goal once thru a random search. 
https://cs.stackexchange.com/questions/63878/is-it-possible-to-solve-the-mountain-car-reinforcement-learning-task-with-linear

Dueling networks: have a network with two “heads” estimating V(s) and A(s,a)=Q(s,a)-V(s), and combine them into a Q function
* Intuition behind this working: kind of like how resnets work well because learning zero function is way easier than learning identity? 
    * Also because the state-value function is updated at every  step, instead of just state-action value function for the chosen action
* see page 11 of paper 4 for algorithm

Why does Alexnet require square images? (claim in first atari paper) Is this more a property of CNNs in general? 

### Atari implementation

1. raw frames converted to grayscale, 110x84 image. crop 84x84 region that roughly captures playing area
2. Stack last 4 frames together to be input to Q-function (84x84x8) 
3. Network: 
    1. conv1: 16 filters of 8x8, stride 4, relu
    2. conv2: 32 filters of 4x4, stride 2, relu
    3. (conv3, nature paper): 64 filters of 3x3, stride 1, relu
    4. fc: 256 relu (512 in nature paper)
    5. output: n_actions

Dueling network:

1. conv1: 32 8x8 filters, stride 4, relu
2. conv2: 64 4x4 filters, stride 2, relu
3. conv3: 64 3x3 filters, stride 1, relu
4. Split: 
    * fc value: 512 units, 1 output, linear activation
    * fc advantage: 512 units, n-actions outputs, linear activation
5. Merge to make Q: 
    * Q = V + (A - mean(A))

Training: 

* rmsprop with minibatches of size 32. 
* epsilon annealed linearly from 1 to 0.1 over first million frames, constant 0.1 after
* clip rewards to 1, -1, or 0
* Train for 10 million frames
* replay memory: 1 million most recent frames
* frame-skipping: every third frame for space invaders
    * last action repeated on skipped frames
* preprocessing for nature atari paper: 
    * to remove flickering: take max value for each pixel colour over frame at t and t-1
    * take Y channel (luminance) from RGB frame, rescale to 84x84. Why luminance, and not another hcnnel? 
    * stack four together
* see pg10 of nature paper for list of hyperparameters

### Performance logs


### References

* https://github.com/williamFalcon/DeepRLHacks
* https://sites.google.com/view/deep-rl-bootcamp/lectures

