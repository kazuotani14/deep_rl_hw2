import numpy as np
import matplotlib.pyplot as plt

perf_cartpole_linear_replay = np.array([9.25, 18.05, 39.95, 41.5, 70.45, 90.4, 103.75, 119.75, 113.4, 123.45, 127.1, 138.9, 146.1, 150.2, 163.15, 171.15, 191.7, 192.6, 200.0])
perf_cartpole_dqn = np.array([9.5, 9.5, 9.7, 99.85, 31.85, 31.55, 30.65, 16.05, 70.4, 12.9, 40.65, 63.9, 46.45, 63.3, 199.7])
perf_cartpole_dueling = np.array([9.55, 9.3, 39.8, 49.8, 40.6, 49.25, 40.8, 39.65, 33.7, 150.9, 134.45, 148.6, 140.25, 116.85, 200.0])
perf_mountaincar_dqn = np.array([-200.0, -200.0, -200.0, -200.0, -200.0, -200.0, -200.0, -200.0, -200.0, -200.0, -200.0, -184.95, -200.0, -200.0, -142.5, -121.15, -119.1, -118.05, -113.85, -107.75])
perf_mountaincar_dqn_remember = np.array([-200.0, -200.0, -200.0, -200.0, -200.0, -200.0, -200.0, -200.0, -200.0, -200.0, -156.35, -148.75, -176.55, -160.85, -135.8, -151.9, -108.75])
perf_mountaincar_dueling_remember = np.array([-200.0, -200.0, -200.0, -200.0, -200.0, -200.0, -200.0, -200.0, -200.0, -200.0, -200.0, -200.0, -200.0, -200.0, -184.65, -200.0, -200.0, -196.5, -200.0, -200.0, -142.8, -172.3, -135.85, -133.35, -155.2, -193.35, -113.65, -112.05, -113.2, -125.2, -116.7, -104.9])

perf_mountaincar_linear = np.array([-200]*int(3000/50))

perf_to_plot = perf_mountaincar_linear
episodes = np.arange(0, len(perf_to_plot)) * 50

plt.plot(episodes, perf_to_plot)
plt.xlabel('Episodes')
plt.ylabel('Performance (average reward over 20 episodes)')
plt.title('Performance of linear Q-network on mountaincar')
plt.savefig('mountaincar_linear.png', bbox_inches='tight')
plt.show()
