DQN_Implementation.py:

Trains or tests a Q-network on the CartPole-v0 or MountainCar-v0 OpenAI Gym environment.

Usage:

Python3 DQN_Implementation.py [options]

Options:

--env (string)                  ['CartPole-v0', 'MountainCar-v0'] name of OpenAI Gym environment to use
--model_type (string)           ['linear', 'dqn_1hidden', 'dqn_2hidden', 'dqn_3hidden', 'dueling_dqn'] network architecture
--learning_rate (float)         learning rate used during training
--epsilon_start (float)         beginning value of epsilon which decays over 100000 iterations to epsilon_end
--epsilon_end (float)           ending value of epsilon which decays from epsilon_start over 100000 iterations                  

--train (bool)                  [0, 1], default = 1. 0 if used to train a network, 1 if only testing a network
--render (bool)                 [0, 1], default = 0. 1  to render the environment, otherwise 0
--model_path (string)           path to specific tensorflow model to be used
--activations (string)          default = 'relu', activation used in hidden layers
--replay_freq (string)          [none, 'every_iter', 'every_episode'], default = 'every_iter'. We experimented with 
                                training from memory with experience replay every iteration and every episode. experience replay
                                can be turned off when set to none.


