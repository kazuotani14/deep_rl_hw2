"""
Testing random search of linear policies from: 
http://kvfrans.com/simple-algoritms-for-solving-cartpole/

Turns out both tasks can be solved with a linear Q-function...
"""

import gym
import numpy as np

env = gym.make('CartPole-v0')
# env = gym.make('MountainCar-v0')

state_size = env.observation_space.shape[0]
action_size = env.action_space.n 

best_params = None  
best_reward = -200

n_episodes = 5000
for i in range(n_episodes):
    obs = env.reset()

    Q = np.random.rand(action_size, state_size) * 2 - 1 # weights -1,1
    total_reward = 0

    for j in range(200):
        # Show solved policy, first best_reward is for cartpole, second for mountain car
        solved = (best_reward==200) or (best_reward<0 and best_reward > -200)
        if solved: 
            Q = best_params
            if (j==0): print('Solved! ', Q)
            env.render()

        action = np.argmax(np.matmul(Q, obs))

        obs, reward, done, info = env.step(action)
        total_reward += reward
        if done:
            break

    if total_reward > best_reward:
        best_reward = total_reward
        best_params = Q
    print('Episodes: {}/{}. Rewards: {}, {}'.format(i, n_episodes, total_reward, best_reward))
