import gym
import numpy as np
import keras
from keras.models import load_model

def run_policy(model_path, env_name, record_path=None):
    """
    Loads a model and runs it. 
    """
    env = gym.make(env_name)
    if record_path is not None: 
        env = gym.wrappers.Monitor(env, record_path, force=True)

    model = load_model(model_path)

    n_episodes = 2

    episode_rewards = []
    for _ in range(n_episodes):
        total_reward = 0
        done = False
        obs = env.reset()

        while not done:
            # env.render()

            action = np.argmax(model.predict(np.array([obs])))

            # if np.random.rand() > 0.05:
                # action = np.argmax(model.predict(np.array([obs])))
            # else:
                # action = env.action_space.sample()

            obs, reward, done, info = env.step(action)
            total_reward += reward 

        episode_rewards.append(total_reward)

    episode_rewards = np.array(episode_rewards)
    print(episode_rewards)
    print(np.mean(episode_rewards), np.std(episode_rewards))

if __name__ == "__main__":
    # model_path = 'models/cartpole_dueling_dqn/dqn_dueling-CartPole-v0-episode-650.h5'
    # model_path = 'models/dqn_2hidden-MountainCar-v0-687-episode-100.h5'
    model_path = 'models/linear-MountainCar-v0-0.001-1.0-0.01-0.h5'

    # env_name = 'CartPole-v0'
    env_name = 'MountainCar-v0'

    # record_path = None # Don't record, just render
    record_path = 'models/'

    run_policy(model_path, env_name, record_path)